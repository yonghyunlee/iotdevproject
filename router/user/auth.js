const userModel = require('model/userSchema');
const jwt = require('jsonwebtoken');
const secret = 'IOTProject-secret';

exports.login = async (req, res) => {
  try {
    const { id, password } = req.body;

    const user = await userModel.findOne({id: id, password: password});

    if (user) {
      const token = await jwt.sign(
        {
          id: id,
        }, secret, {
          expiresIn: '1d'
        });

      return res.json({
        message: 'logged in successfully',
        token : token,
        id: id
      })
    } else {
      console.error("incorrect id or password");

      return res.status(500).json({
        message: "incorrect id or password"
      });
    }
  } catch (e) {
    console.error(e);
    res.sendStatus(500);
  }
};

exports.register = async (req, res) => {
  try {
    const { id, password } = req.body;

    const user = await userModel.find({id: id});

    if (user.length > 0) {
      console.error("id exists");

      return res.status(500).json({
        message: "id exists"
      });
    }

    const newUser = await new userModel({
      id: id,
      password: password
    });

    newUser.save();

    res.sendStatus(200);
  } catch (e) {
    console.error(e);
    res.sendStatus(500);
  }
};