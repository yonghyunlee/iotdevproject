const express = require('express');
const router = express.Router();
const auth = require('./auth');

router.post('/login', auth.login);
router.post('/registration', auth.register);

module.exports = router;