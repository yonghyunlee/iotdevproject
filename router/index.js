const express = require('express');
const bodyParser = require('body-parser');

const router = express.Router();
const user = require('./user');

router.use(bodyParser.urlencoded({ extended:  false }));
router.use(bodyParser.json());
router.use('/user', user);

module.exports = router;