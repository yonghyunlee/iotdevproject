const express = require('express');
const app = express();
const api = require('./router');
const cors = require('cors');
const port = 3000;
const { connect } = require('./mongoDB');
connect();
// CORS 설정
app.use(cors());
app.set('views', '/views/ejs');
app.use(express.static('public'));
app.use("/api", api);

app.get('/*', (req, res) => {
  res.send("Hello world!")
});

app.listen(port, () => {
  console.log("connected 3000 port!")
});