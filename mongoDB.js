const mongoose = require('mongoose');
const mongoDBUrl = 'mongodb://localhost/IOTDev';

const connect = () => {

  mongoose.connect(mongoDBUrl, {
    useNewUrlParser: true,
    useCreateIndex: true,
  });

  const db = mongoose.connection;
  db.on('error', console.error.bind(console, 'connection error:'));

  db.once('open', function() {
    console.log('mongoDB connected')
  });
};

module.exports = {
  connect
};