const jwt = require('jsonwebtoken');
const secret = 'IOTProject-secret';

const authMiddleware = (req, res, next) => {
  try {
    // header에서 token 가져옴
    const token = req.headers['x-access-token'];

    // token이 없다면
    if(!token) {
      return new Error("not login")
    }

    req.decoded = jwt.verify(token, secret);

    next();

  } catch (e) {
    console.error(e);
    res.sendStatus(403);
  }
};

module.exports = authMiddleware;