const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const userSchema = new Schema({
  id: {
    type: String,
    min: 2,
    max: 15,
    lowercase: true,
    required: true,
    unique: true
  },
  password: {
    type: String,
    min: 2,
    max: 15,
    required: true,
    unique: true
  }
});

module.exports = mongoose.model('user', userSchema);